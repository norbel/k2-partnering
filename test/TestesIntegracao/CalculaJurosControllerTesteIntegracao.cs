﻿using Api2.Controllers;
using Api2.Interfaces;
using Api2.Servico;
using Api2.Validador;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace TestesUnitarios
{
    public class CalculaJurosControllerTesteIntegracao
    {
        public CalculaJurosControllerTesteIntegracao()
        {
        }

        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(1, 0, 1)]
        [InlineData(1, 1, 1.01)]
        [InlineData(12345, 1, 12468.45)]
        [InlineData(1, 12345, 2.225150277252523E+53)]
        [InlineData(7, 9, 7.65)]
        [InlineData(3.6, 12, 4.05)]
        [InlineData(4.3, 24, 5.45)]
        [InlineData(-1, 0, -1)]
        [InlineData(-1, 1, -1.01)]
        [InlineData(-12345, 1, -12468.45)]
        [InlineData(-1, 12345, -2.225150277252523E+53)]
        [InlineData(-7, 9, -7.65)]
        [InlineData(-3.6, 12, -4.05)]
        [InlineData(-4.3, 24, -5.45)]
        [InlineData(double.MinValue, 1, Double.NegativeInfinity)]
        [InlineData(double.MaxValue, 1, Double.PositiveInfinity)]
        public async Task Sucesso_Quando_Tempo_For_Nao_Negativo(double valorInicial, int tempo, double resultado)
        {
            //Arrange
            HttpContent content = new StringContent("0.01");
            var consultaApiMock = new Mock<IConsultaApi>();
            consultaApiMock.Setup(m => m.ConsultaApiAsync(string.Empty, string.Empty))
                .Returns(Task.FromResult(new HttpResponseMessage { Content = content, StatusCode = HttpStatusCode.OK }));

            var configurationMock = new Mock<IConfiguration>();
            configurationMock.Setup(m => m["ConsultaTaxaJuros:Url"]).Returns(string.Empty);
            configurationMock.Setup(m => m["ConsultaTaxaJuros:Recurso"]).Returns(string.Empty);

            var servico = new CalculaJurosServico(configurationMock.Object, consultaApiMock.Object);

            var controller = new CalculaJurosController(servico);
            var validador = new CalculaJurosValidador { Tempo = tempo, ValorInicial = valorInicial };

            //Act
            var resposta = await controller.CalculaJurosAsync(validador);
            var valorFinal = resposta.Result as OkObjectResult;

            //Assert
            Assert.Equal(resultado, valorFinal.Value);
        }
    }
}
