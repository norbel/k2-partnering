﻿using Api2.Interfaces;
using Api2.Servico;
using Microsoft.Extensions.Configuration;
using Moq;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace TestesUnitarios
{
    public class CalculaJurosServicoTesteUnitario
    {
        public CalculaJurosServicoTesteUnitario()
        {
        }

        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(1, 0, 1)]
        [InlineData(1, 1, 1.01)]
        [InlineData(12345, 1, 12468.45)]
        [InlineData(1, 12345, 2.225150277252523E+53)]
        [InlineData(7, 9, 7.65)]
        [InlineData(3.6, 12, 4.05)]
        [InlineData(4.3, 24, 5.45)]
        [InlineData(-1, 0, -1)]
        [InlineData(-1, 1, -1.01)]
        [InlineData(-12345, 1, -12468.45)]
        [InlineData(-1, 12345, -2.225150277252523E+53)]
        [InlineData(-7, 9, -7.65)]
        [InlineData(-3.6, 12, -4.05)]
        [InlineData(-4.3, 24, -5.45)]
        [InlineData(double.MinValue, 1, Double.NegativeInfinity)]
        [InlineData(double.MaxValue, 1, Double.PositiveInfinity)]
        public async Task Sucesso_No_Calculo_Para_Diferentes_Valores(double valorInicial, int tempo, double valorFinal)
        {
            //Arrange
            HttpContent content = new StringContent("0.01");
            var consultaApiMock = new Mock<IConsultaApi>();
            consultaApiMock.Setup(m => m.ConsultaApiAsync(string.Empty, string.Empty))
                .Returns(Task.FromResult(new HttpResponseMessage { Content = content, StatusCode = HttpStatusCode.OK }));

            var configurationMock = new Mock<IConfiguration>();
            configurationMock.Setup(m => m["ConsultaTaxaJuros:Url"]).Returns(string.Empty);
            configurationMock.Setup(m => m["ConsultaTaxaJuros:Recurso"]).Returns(string.Empty);

            var servico = new CalculaJurosServico(configurationMock.Object, consultaApiMock.Object);

            //Act
            var calculoValorFinal = await servico.CalculaJurosAsync(valorInicial, tempo);

            //Assert
            Assert.Equal(valorFinal, calculoValorFinal);
        }

        [Fact]
        public async Task Erro_Quando_Nao_Encontrar_Configuracao()
        {
            //Arrange
            var rand = new Random();
            double valorInicial = rand.NextDouble();
            int tempo = rand.Next(0, Int32.MaxValue);

            HttpContent content = new StringContent("0.01");
            var consultaApiMock = new Mock<IConsultaApi>();
            consultaApiMock.Setup(m => m.ConsultaApiAsync(string.Empty, string.Empty))
                .Returns(Task.FromResult(new HttpResponseMessage { Content = content, StatusCode = HttpStatusCode.OK }));

            var configurationMock = new Mock<IConfiguration>();
            configurationMock.Setup(m => m["ConsultaTaxaJuros"]).Returns(string.Empty);
            configurationMock.Setup(m => m["RecursoTeste"]).Returns(string.Empty);

            var servico = new CalculaJurosServico(configurationMock.Object, consultaApiMock.Object);

            //Act

            //Assert
            await Assert.ThrowsAsync<Exception>(() => servico.CalculaJurosAsync(valorInicial, tempo));
        }

        [Fact]
        public async Task Erro_Quando_Consulta_Api_Estiver_Falhando()
        {
            //Arrange
            var rand = new Random();
            double valorInicial = rand.NextDouble();
            int tempo = rand.Next(0, Int32.MaxValue);

            HttpContent content = new StringContent("0.01");
            var consultaApiMock = new Mock<IConsultaApi>();
            consultaApiMock.Setup(m => m.ConsultaApiAsync(string.Empty, string.Empty))
                .ThrowsAsync(new Exception());

            var configurationMock = new Mock<IConfiguration>();
            configurationMock.Setup(m => m["ConsultaTaxaJuros:Url"]).Returns(string.Empty);
            configurationMock.Setup(m => m["ConsultaTaxaJuros:Recurso"]).Returns(string.Empty);

            var servico = new CalculaJurosServico(configurationMock.Object, consultaApiMock.Object);

            //Act

            //Assert
            await Assert.ThrowsAsync<Exception>(() => servico.CalculaJurosAsync(valorInicial, tempo));
        }
    }
}
