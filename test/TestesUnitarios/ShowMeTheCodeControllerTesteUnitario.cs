using Api2.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace TestesUnitarios
{
    public class ShowMeTheCodeControllerTesteUnitario
    {
        private readonly ShowMeTheCodeController Controller;
        private const string UrlGitProjeto = "https://gitlab.com/norbel/k2-partnering";

        public ShowMeTheCodeControllerTesteUnitario()
        {
            Controller = new ShowMeTheCodeController();
        }

        [Fact]
        public void Sucesso_Quando_Url_Git_Do_Projeto_Esta_Correta()
        {
            //Arrange
            string resultadoCorreto = UrlGitProjeto;

            //Act
            var resposta = Controller.Get().Result;
            var resultado = resposta as OkObjectResult;

            //Assert
            Assert.Equal(resultadoCorreto, resultado.Value);
        }
    }
}
