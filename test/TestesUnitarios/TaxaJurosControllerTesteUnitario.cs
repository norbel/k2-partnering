using Api1.Controllers;
using Microsoft.AspNetCore.Mvc;
using Xunit;

namespace TestesUnitarios
{
    public class TaxaJurosControllerTesteUnitario
    {
        private readonly TaxaJurosController Controller;

        public TaxaJurosControllerTesteUnitario()
        {
            Controller = new TaxaJurosController();
        }

        [Fact]
        public void Sucesso_Quando_Resultado_API1_Igual_Um_Porcento()
        {
            //Arrange
            double resultado = 0.01;

            //Act
            var resposta = Controller.Get();
            var valorFinal = resposta.Result as OkObjectResult;

            //Assert
            Assert.Equal(resultado, valorFinal.Value);
        }
    }
}
