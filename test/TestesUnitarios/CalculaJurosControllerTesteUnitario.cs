﻿using Api2.Controllers;
using Api2.Interfaces;
using Api2.Validador;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace TestesUnitarios
{
    public class CalculaJurosControllerTesteUnitario
    {
        public CalculaJurosControllerTesteUnitario()
        {
        }

        [Theory]
        [InlineData(1, 1, 1.01)]
        [InlineData(-1, 1, -1.01)]
        [InlineData(double.MinValue, 1, Double.NegativeInfinity)]
        [InlineData(double.MaxValue, 1, Double.PositiveInfinity)]
        public async Task Sucesso_Quando_Tempo_For_Nao_Negativo(double valorInicial, int tempo, double resultado)
        {
            //Arrange
            var servicoMock = new Mock<ICalculaJurosServico>();
            servicoMock.Setup(m => m.CalculaJurosAsync(valorInicial, tempo)).Returns(Task.FromResult(resultado));
            var controller = new CalculaJurosController(servicoMock.Object);
            var validador = new CalculaJurosValidador { Tempo = tempo, ValorInicial = valorInicial };

            //Act
            var resposta = await controller.CalculaJurosAsync(validador);
            var valorFinal = resposta.Result as OkObjectResult;

            //Assert
            Assert.Equal(resultado, valorFinal.Value);
        }

        [Theory]
        [InlineData(1, -1)]
        [InlineData(12345, -1)]
        [InlineData(1, -12345)]
        [InlineData(7, -9)]
        [InlineData(3.6, -12)]
        [InlineData(4.3, -24)]
        [InlineData(double.MinValue, -1)]
        [InlineData(double.MaxValue, -1)]
        public async Task Erro_Quando_Tempo_For_Negativo(double valorInicial, int tempo)
        {
            //Arrange
            var servicoMock = new Mock<ICalculaJurosServico>();
            var controller = new CalculaJurosController(servicoMock.Object);
            var validador = new CalculaJurosValidador { Tempo = tempo, ValorInicial = valorInicial };

            //Act
            var excessao = await controller.CalculaJurosAsync(validador);

            //Assert
            Assert.IsType<BadRequestObjectResult>(excessao.Result);
        }
    }
}
