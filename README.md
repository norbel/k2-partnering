# Desafio K2 Partnering

Teste técnico .NET

# Links de referência

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [GitIgnore](https://github.com/dotnet/core/blob/master/.gitignore)

## Projeto

O projeto contém com duas API's como solicitado da descrição do desafio.

Adicionei um arquivo `.gitignore` e o `.gitlab-ci.yml` obtidos pelos links de referência.
