﻿using System.Threading.Tasks;

namespace Api2.Interfaces
{
    public interface ICalculaJurosServico
    {
        Task<double> CalculaJurosAsync(double valorInicial, int tempo);
    }
}
