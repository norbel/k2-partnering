﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Api2.Interfaces
{
    public interface IConsultaApi
    {
        Task<HttpResponseMessage> ConsultaApiAsync(string url, string recurso);
    }
}