﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Api2.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ShowMeTheCodeController : ControllerBase
    {
        private const string Descricao = "Sempre retorna a Url do git do projeto";
        private readonly string UrlGitProjeto = "https://gitlab.com/norbel/k2-partnering";

        [HttpGet]
        [SwaggerResponse(StatusCodes.Status200OK, Descricao)]
        public ActionResult<string> Get()
        {
            return Ok(UrlGitProjeto);
        }
    }
}
