﻿using Api2.Interfaces;
using Api2.Properties;
using Api2.Validador;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading.Tasks;

namespace Api2.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CalculaJurosController : ControllerBase
    {
        private readonly ICalculaJurosServico Servico;
        private const string DescricaoCalculaJuros = "Calcula juros composto.";

        public CalculaJurosController(ICalculaJurosServico servico)
        {
            Servico = servico;
        }

        [HttpPost]
        [SwaggerResponse(StatusCodes.Status200OK, DescricaoCalculaJuros)]
        [SwaggerResponse(StatusCodes.Status400BadRequest)]
        [SwaggerResponse(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<double>> CalculaJurosAsync([FromQuery] CalculaJurosValidador calculaJuros)
        {
            if (!calculaJuros.IsValid())
                return BadRequest(calculaJuros.Validate());

            try
            {
                double resultado = await Servico.CalculaJurosAsync(calculaJuros.ValorInicial.Value, calculaJuros.Tempo.Value);
                return Ok(resultado);
            }
            catch (Exception)
            {
                string mensagemErro = Resources.Erro;
                return StatusCode(StatusCodes.Status500InternalServerError, mensagemErro);
            }
        }
    }
}