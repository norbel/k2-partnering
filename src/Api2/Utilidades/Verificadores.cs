﻿using System;

namespace Api2.Utilidades
{
    public static class Verificadores
    {
        public static bool VerificaUrlValida(this string url)
        {
            return Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute); ;
        }
    }
}
