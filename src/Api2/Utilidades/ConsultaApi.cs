﻿using Api2.Interfaces;
using Api2.Properties;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Api2.Utilidades
{
    public class ConsultaApi : IConsultaApi
    {
        public async Task<HttpResponseMessage> ConsultaApiAsync(string url, string recurso)
        {
            try
            {
                using (HttpClient cliente = new HttpClient())
                {
                    cliente.BaseAddress = new Uri(url);
                    cliente.DefaultRequestHeaders.Accept.Clear();

                    HttpResponseMessage resposta = await cliente.GetAsync(recurso);

                    if (resposta.IsSuccessStatusCode)
                    {
                        return resposta;
                    }
                    else
                    {
                        throw new Exception(string.Concat(Resources.ErroChamadaApi, url, recurso));
                    }
                }
            }
            catch (Exception excessao)
            {
                throw excessao;
            }
        }
    }
}
