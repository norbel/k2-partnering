﻿using Api2.Properties;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Api2.Validador
{
    public class CalculaJurosValidador
    {
        public double? ValorInicial { get; set; }

        public int? Tempo { get; set; }

        public IEnumerable<ValidationResult> Validate()
        {
            if (ValorInicial == null)
            {
                yield return new ValidationResult(
                    string.Format(Resources.ValorInicialObrigatorio, nameof(ValorInicial)),
                    new[] { nameof(ValorInicial) });
            }
            if (Tempo == null)
            {
                yield return new ValidationResult(
                    string.Format(Resources.TempoObrigatorio, nameof(Tempo)),
                    new[] { nameof(Tempo) });
            }
            else if (Tempo < 0)
            {
                yield return new ValidationResult(
                    string.Format(Resources.ErroTempoNegativo, nameof(Tempo)),
                    new[] { nameof(Tempo) });
            }
        }

        public bool IsValid()
        {
            return this.Validate().ToList().Count == 0;
        }
    }
}
