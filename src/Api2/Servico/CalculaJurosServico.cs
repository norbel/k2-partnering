﻿using Api2.Interfaces;
using Api2.Utilidades;
using Microsoft.Extensions.Configuration;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace Api2.Servico
{
    public class CalculaJurosServico : ICalculaJurosServico
    {
        private readonly IConfiguration Configuracao;
        private readonly IConsultaApi ConsultaApi;

        public CalculaJurosServico(IConfiguration configuracao, IConsultaApi consultaApi)
        {
            ConsultaApi = consultaApi;
            Configuracao = configuracao;
        }

        public async Task<double> CalculaJurosAsync(double valorInicial, int tempo)
        {
            try
            {
                var taxaJuros = await ConsultaTaxaJurosAsync();
                var calculo = Math.Pow((1 + taxaJuros), (double)tempo) * valorInicial;
                return Math.Round(calculo, 2, MidpointRounding.ToZero);
            }
            catch (Exception excessao)
            {
                throw excessao;
            }
        }

        private async Task<double> ConsultaTaxaJurosAsync()
        {
            string recurso = Configuracao["ConsultaTaxaJuros:Recurso"];
            string urlConsultaTaxaJuros = Configuracao["ConsultaTaxaJuros:Url"];
            if (urlConsultaTaxaJuros.VerificaUrlValida())
            {
                var resposta = await ConsultaApi.ConsultaApiAsync(urlConsultaTaxaJuros, recurso);
                var taxaJuros = await resposta.Content.ReadAsStringAsync();
                return Convert.ToDouble(taxaJuros, new NumberFormatInfo { NumberDecimalSeparator = "." });
            }
            else
            {
                throw new Exception();
            }
        }
    }
}
