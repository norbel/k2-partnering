﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Api1.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TaxaJurosController : ControllerBase
    {
        private const string DescricaoTaxaJuros = "Sempre retorna a taxa de juros de 1%, ou seja, 0.01";

        [HttpGet]
        [SwaggerResponse(StatusCodes.Status200OK, DescricaoTaxaJuros)]
        public ActionResult<double> Get()
        {
            return Ok(0.01);
        }
    }
}
